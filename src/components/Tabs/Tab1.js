import React, { Component } from "react";
import {
  View,
  Text
} from "react-native";

export default class Tab1 extends Component {
  state = {};
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "#49C4A8" }}>
        <Text>Tab1</Text>
      </View>
    );
  }
}
